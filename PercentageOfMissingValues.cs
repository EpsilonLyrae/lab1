﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace NeuroLab1wf
{
    public static class PercentageOfMissingValues
    {
        public static void CreateCSV(DataTable dataTable)
        {
            double tmpVal;
            var lines = new List<string>();
            var amountOfMissingValues = new List<int>();
            string[] columnNames = dataTable.Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName)
                .ToArray();

            lines.Add(string.Join(",", columnNames));
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                amountOfMissingValues.Add(0);
                foreach(DataRow row in dataTable.Rows)
                {
                    if(!double.TryParse(row[i].ToString(), out tmpVal))
                    {
                        amountOfMissingValues[i]++;
                    }
                }
            }
            lines.Add(string.Join(",", amountOfMissingValues));
            File.WriteAllLines("Percentage.csv", lines);
        }
    }
}
