﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Drawing;

namespace NeuroLab1wf
{
    public static class Correlation
    {
        public static void CreateHeatMap(DataTable dataTable)
        {
            double maxElement = double.MinValue;
            double minElement = double.MaxValue;
            DataTable correlationTable = new DataTable();
            List<List<double>> results = new List<List<double>>();
            correlationTable.Columns.Add("-");
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                correlationTable.Columns.Add(dataTable.Columns[i].ColumnName);
            }
            GetCoefMap(dataTable, results);
            Filling(dataTable, correlationTable, results, ref minElement, ref maxElement);
            var dataGridView = CreateDataGridView(correlationTable, minElement, maxElement);
            Export(dataGridView);
            

        }
        private static void Export(DataGridView dataGridView)
        {
            int height = dataGridView.Height;
            dataGridView.Height = dataGridView.RowCount * dataGridView.RowTemplate.Height;

            Bitmap bitmap = new Bitmap(dataGridView.Width, dataGridView.Height);
            dataGridView.DrawToBitmap(bitmap, new Rectangle(0, 0, dataGridView.Width, dataGridView.Height));

            dataGridView.Height = height;

            bitmap.Save("DataGridView.png");
        } 

        private static DataGridView CreateDataGridView(DataTable correlationTable, double minElement, double maxElement)
        {
            var form = new Form()
            {
                WindowState = FormWindowState.Maximized
                
            };
            var dataGridView = new DataGridView()
            {
                Parent = form,
                Dock = DockStyle.Fill,
                AutoGenerateColumns = false
            };
            dataGridView.AutoSize = true;
            dataGridView.RowTemplate.Height = 21;
            dataGridView.DefaultCellStyle.Font = new Font("Tahoma", 7);
            for (int i = 0; i < correlationTable.Columns.Count; i++)
            {
                dataGridView.Columns.Add(correlationTable.Columns[i].ColumnName, correlationTable.Columns[i].ColumnName);
                dataGridView.Columns[i].Width = 40;
            }
            dataGridView.Columns[0].Width = 80;
            foreach (DataRow row in correlationTable.Rows)
            {
                dataGridView.Rows.Add(row.ItemArray);
            }
            for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
            {
                for (int j = 1; j < dataGridView.Columns.Count; j++)
                {
                    dataGridView[j, i].Style.BackColor = HeatMapColor(double.Parse(dataGridView[j, i].Value.ToString()), minElement, maxElement);
                }
            }
            form.ShowDialog();
            return dataGridView;
        }
        private static Color HeatMapColor(double value, double minElement, double maxElement)
        {
            if(value == 0)
            {
                return Color.White;
            }
            
            Color secondColour = Color.Green;
            Color firstColour = Color.White;


            int rOffset = Math.Max(firstColour.R, secondColour.R);
            int gOffset = Math.Max(firstColour.G, secondColour.G);
            int bOffset = Math.Max(firstColour.B, secondColour.B);

            int deltaR = Math.Abs(firstColour.R - secondColour.R);
            int deltaG = Math.Abs(firstColour.G - secondColour.G);
            int deltaB = Math.Abs(firstColour.B - secondColour.B);

            double val = (value - minElement) / (maxElement - minElement);
            int r = rOffset - Convert.ToByte(deltaR * ( val));
            int g = gOffset - Convert.ToByte(deltaG * ( val));
            int b = bOffset - Convert.ToByte(deltaB * ( val));

            return Color.FromArgb(255, r, g, b);
        }

        private static void GetCoefMap(DataTable dataTable, List<List<double>> results)
        {
            List<double> list;
            double coef = 0;
            int numberOfMissingRows;
            int n;
            int NumberOfColumns = dataTable.Columns.Count;
            for (int i = 0; i < NumberOfColumns; i++)
            {
                list = new List<double>();
                for (int j = 0; j < NumberOfColumns; j++)
                {
                    numberOfMissingRows = GetNumberOfMissingRows(dataTable, i, j);
                    n = NumberOfColumns - numberOfMissingRows;
                    coef = (n * SumXY(dataTable, i, j)) - (SumX(dataTable, i, j) * SumX(dataTable, j, i));
                    coef /= Math.Sqrt((n * SumXSquared(dataTable, i, j)) - Math.Pow(SumX(dataTable, i, j), 2))
                        * Math.Sqrt((n * SumXSquared(dataTable, j, i)) - Math.Pow(SumX(dataTable, j, i), 2));
                    list.Add(coef);
                }
                results.Add(list);
            }
        }

        private static int GetNumberOfMissingRows(DataTable dataTable, int numberOfColumn1, int numberOfColumn2)
        {
            double num1, num2;
            int numberOfMissingRows = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                if (!(!Equals(row[numberOfColumn1], null) && double.TryParse(row[numberOfColumn1].ToString(), out num1)
                    && !Equals(row[numberOfColumn2], null) && double.TryParse(row[numberOfColumn2].ToString(), out num2)))
                {
                    numberOfMissingRows++;
                }
            }
            return numberOfMissingRows;
        }

        private static double SumX(DataTable dataTable, int numberOfTargetColumn, int numberOfColumn2)
        {
            double targetNum, num2;
            double sum = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                if (!Equals(row[numberOfTargetColumn], null) && double.TryParse(row[numberOfTargetColumn].ToString(), out targetNum)
                    && !Equals(row[numberOfColumn2], null) && double.TryParse(row[numberOfColumn2].ToString(), out num2))
                {
                    sum += targetNum;
                }
            }
            return sum;
        }
        private static double SumXY(DataTable dataTable, int numberOfColumn1, int numberOfColumn2)
        {
            double num1, num2;
            double sum = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                if (!Equals(row[numberOfColumn1], null) && double.TryParse(row[numberOfColumn1].ToString(), out num1)
                    && !Equals(row[numberOfColumn2], null) && double.TryParse(row[numberOfColumn2].ToString(), out num2))
                {
                    sum += (num1 * num2);
                }
            }
            return sum;
        }
        private static double SumXSquared(DataTable dataTable, int numberOfTargetColumn, int numberOfColumn2)
        {
            double targetNum, num2;
            double sum = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                if (!Equals(row[numberOfTargetColumn], null) && double.TryParse(row[numberOfTargetColumn].ToString(), out targetNum)
                    && !Equals(row[numberOfColumn2], null) && double.TryParse(row[numberOfColumn2].ToString(), out num2))
                {
                    sum += (targetNum * targetNum);
                }
            }
            return sum;
        }
        private static void Filling(DataTable dataTable, DataTable correlationTable, List<List<double>> results, ref double minElement, ref double maxElement)
        {
            double tmp;
            for (int i = 0; i < correlationTable.Columns.Count-1; i++)
            {
                DataRow row = correlationTable.NewRow();
                row[0] = dataTable.Columns[i].ColumnName;
                for (int j = 0; j < correlationTable.Columns.Count-1; j++)
                {
                    tmp = results[i][j];
                    row[j + 1] = tmp;
                    if(tmp > maxElement)
                    {
                        maxElement = tmp;
                    }
                    if(tmp < minElement && tmp != 0)
                    {
                        minElement = tmp;
                    }
                }
                correlationTable.Rows.Add(row);
            }
            
        }
    }
}
