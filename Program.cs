using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace NeuroLab1wf
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dataTable;
            dataTable = XlsxFileWorker.ReadDataExcel(args[0]);
            DataCorrector.CorrectData(dataTable);
            ///////////////////////////////////////////
            Correlation.CreateHeatMap(dataTable);
            ///////////////////////////////////////////
            Histogram.CreateCSV(dataTable);
            ///////////////////////////////////////////
            PercentageOfMissingValues.CreateCSV(dataTable);
            ///////////////////////////////////////////
            NumberOfUniqueElements.CreateCSV(dataTable);
            GainRatio.CreateCSV(dataTable);
        }
    }
}
