﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace NeuroLab1wf
{
    static class DataCorrector
    {
        public static void CorrectData(DataTable dataTable)
        {
            RemoveExtraColumns(dataTable);
            DefineHeaders(dataTable);
            RemoveExtraRows(dataTable);
            CastKGF(dataTable);
        }
        private static void RemoveExtraColumns(DataTable dataTable)
        {
            dataTable.Columns.RemoveAt(0);//remove №
            dataTable.Columns.RemoveAt(0);//remove date
        }
        private static void DefineHeaders(DataTable dataTable)
        {
            var row = dataTable.Rows[0];
            var columns = dataTable.Columns;
            for (int i = 0; i < columns.Count; i++)
            {
                if (!dataTable.Columns.Contains((string)row[i])){
                    columns[i].ColumnName = (string)row[i];
                }
                else
                {
                    columns[i].ColumnName = (string)row[i] + " ";
                }
            }
        }
        private static void RemoveExtraRows(DataTable dataTable)
        {
            List<DataRow> deleteIt = new List<DataRow>();
            dataTable.Rows.RemoveAt(0);
            dataTable.Rows.RemoveAt(0);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                var dataRow = dataTable.Rows[i];
                if ((DBNull.Value.Equals(dataRow.ItemArray[dataTable.Columns.Count - 1]) &&
                    DBNull.Value.Equals(dataRow.ItemArray[dataTable.Columns.Count - 2])) ||
                    DBNull.Value.Equals(dataRow.ItemArray[dataTable.Columns.Count - 3]))
                {
                    deleteIt.Add(dataRow);
                }

            }
            foreach (var el in deleteIt)
            {
                dataTable.Rows.Remove(el);
            }
        }

        private static void CastKGF(DataTable dataTable)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                var dataRow = dataTable.Rows[i];
                if(!DBNull.Value.Equals(dataRow.ItemArray[dataTable.Columns.Count - 1]))
                {
                    dataRow[dataTable.Columns.Count - 2] = (double)dataRow[dataTable.Columns.Count - 1] * 1000;
                }
            }
            dataTable.Columns.RemoveAt(dataTable.Columns.Count - 1);
        }
    }
}
