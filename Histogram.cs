﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace NeuroLab1wf
{
    public static class Histogram
    {
        public static void CreateCSV(DataTable dataTable)
        {
            var lines = new List<string>();
            double tmpVal;
            string[] columnNames = dataTable.Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName)
                .ToArray();

            lines.Add(string.Join(",", columnNames));
            var valueLines = dataTable.AsEnumerable()
                .Select(row => string.Join(",", row.ItemArray.Select(val => double.TryParse(val.ToString(), out tmpVal) ? tmpVal.ToString() : "" )));

            lines.AddRange(valueLines);

            File.WriteAllLines("Histo.csv", lines);
        } 
    }
}
