﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace NeuroLab1wf
{
    public static class GainRatio
    {
        public static void CreateCSV(DataTable dataTable)
        {
            var mainEntropy = GetMainEntropy(dataTable);
            var result = createGainRatioArray(dataTable, mainEntropy);
            foreach(var el in result)
            {
                Console.WriteLine(el);
            }
            File.WriteAllLines("GainRatio.csv", result);
        }
        public static double GetMainEntropy(DataTable dataTable)
        {
            double tmpVal;
            var classesValue = new Dictionary<double, int>();
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                if (dataTable.Columns[i].ColumnName.Equals("КГФ"))
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        tmpVal = double.Parse(row.ItemArray[i].ToString());
                        if (!classesValue.ContainsKey(tmpVal))
                        {
                            classesValue.Add(tmpVal, 1);
                        }
                        else
                        {
                            classesValue[tmpVal]++;
                        }
                    }
                }
            }
            double entropy = 0;
            double probability = 0;
            foreach (var el in classesValue)
            {
                probability = (double)el.Value / classesValue.Count;
                entropy += probability * Math.Log2(probability);
            }
            return -entropy;
        }

        private static double GetAttributeEntropy(DataTable dataTable, int attributeIndex, string attributeValue)
        {
            double tmpVal;
            var classesValue = new Dictionary<double, int>();
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                if (dataTable.Columns[i].ColumnName.Equals("КГФ"))
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        if (row[attributeIndex].Equals(attributeValue))
                        {
                            tmpVal = double.Parse(row.ItemArray[i].ToString());
                            if (!classesValue.ContainsKey(tmpVal))
                            {
                                classesValue.Add(tmpVal, 1);
                            }
                            else
                            {
                                classesValue[tmpVal]++;
                            }
                        }
                    }
                }
            }
            double entropy = 0;
            double probability = 0;
            foreach (var el in classesValue)
            {
                probability = (double)el.Value / classesValue.Count;
                entropy += probability * Math.Log2(probability);
            }
            return -entropy;
        }

        private static List<string> createGainRatioArray(DataTable dataTable, double mainEntropy)
        {
            string tmpVal;
            double tmpRes;
            var result = new List<string>();
            for (int i = 0; i < dataTable.Columns.Count - 2; i++)
            {
                var attributeValues = new Dictionary<string, int>();
                foreach (DataRow row in dataTable.Rows)
                {
                    tmpVal = row.ItemArray[i].ToString();
                    if (!attributeValues.ContainsKey(tmpVal))
                    {
                        attributeValues.Add(tmpVal, 1);
                    }
                    else
                    {
                        attributeValues[tmpVal]++;
                    }
                }
                double gainPart = 0;
                double splitInfo = 0;
                double probability = 0;
                foreach (var el in attributeValues)
                {
                    probability = (double)el.Value / (dataTable.Columns.Count - 2);
                    gainPart += probability * GetAttributeEntropy(dataTable, i, el.Key);
                    splitInfo += probability * Math.Log2(probability);
                }
                
                splitInfo *= -1;
                if(splitInfo == 0)
                {
                    result.Add("-1");
                }
                else
                {
                    tmpRes = ((mainEntropy - gainPart) / splitInfo);
                    result.Add(tmpRes.ToString());
                }
            }

            return result;
        }

    }
}
