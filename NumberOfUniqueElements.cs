﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace NeuroLab1wf
{
    public static class NumberOfUniqueElements
    {
        public static void CreateCSV(DataTable dataTable)
        {
            double tmpVal;
            var lines = new List<string>();
            var amountOfUniqueValues = new List<int>();
            string[] columnNames = dataTable.Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName)
                .ToArray();

            lines.Add(string.Join(",", columnNames));
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var uniqueObjects = dataTable
                     .AsEnumerable()
                     .Select(row => row[i])
                     .Distinct()
                     .ToList()
                     .Where(val => double.TryParse(val.ToString(), out tmpVal));
                amountOfUniqueValues.Add(uniqueObjects.Count());
            }
            lines.Add(string.Join(",", amountOfUniqueValues));
            File.WriteAllLines("UniqueElements.csv", lines);
        }
    }
}
